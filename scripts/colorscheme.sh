#!/bin/sh

echo "Running"

if [[ -z "$1" ]]
then
  COLOR="random_dark"
else
  COLOR="$1"
fi

wal --theme $COLOR
