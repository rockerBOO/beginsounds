provider "linode" {
  token = var.linode_token
}

data "linode_object_storage_cluster" "primary" {
  id = "us-east-1"
}

resource "linode_object_storage_bucket" "mygeoangelfirespace" {
  cluster = data.linode_object_storage_cluster.primary.id
  label = "beginworld"
}

resource "linode_object_storage_bucket" "scavenger_bucket" {
  cluster = data.linode_object_storage_cluster.primary.id
  label = "scavenger-bucket"
}

resource "linode_object_storage_key" "beginsounds_key" {
  label = "beginsounds-api"

  bucket_access {
    bucket_name = linode_object_storage_bucket.scavenger_bucket.label
    cluster = data.linode_object_storage_cluster.primary.id
    permissions = "read_only"
  }
}
