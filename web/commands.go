package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
)

type CommandPage struct {
	Domain    string
	Extension string
	Name      string
	Cost      int
	Owners    []string
	stream_command.StreamCommand
	Metadata
}

type CommandsPage struct {
	Domain    string
	Extension string
	Commands  []stream_command.StreamCommand
	Metadata
}

func CommandsHandler(w http.ResponseWriter, r *http.Request) {
	commands := stream_command.All(db)
	w.WriteHeader(http.StatusOK)

	// Can We do this in a routine?
	// Should we do this in a Go Routine?
	// Should we upload the page?
	// Should we have something else upload the pages?
	remotePage := CommandsPage{
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
		Commands:  commands,
		Metadata:  metadata,
	}
	buildFile := fmt.Sprintf("build/commands.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = commands_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := CommandsPage{
		Domain:   "http://localhost:1992",
		Commands: commands,
		Metadata: metadata,
	}
	commands_tmpl.Execute(w, page)
}

type CommandResult struct {
	Name    string
	Players []string
	// Owners []*string `gorm:"type:array"`
}

func CommandHandler2(w http.ResponseWriter, r *http.Request) {
	dat, err := ioutil.ReadFile("media2.txt")
	if err != nil {
		fmt.Printf("Error reading missing files: %+v\n", err)
	}
	res := bytes.Split(dat, []byte{'\n'})

	allFiles := stream_command.AllFiles(db)
	for _, filename := range allFiles {
		match := false

		for _, r := range res {
			if filename == string(r) {
				match = true
			}
		}

		if !match {
			var extension = filepath.Ext(filename)
			var commandName = filename[0 : len(filename)-len(extension)]
			sc := stream_command.Find(db, commandName)

			var result []string
			tx := db.Raw(`
		SELECT
			p.name
		FROM
			stream_commands sc
		INNER JOIN
			commands_players cp
				ON
			sc.ID = cp.stream_command_id
			INNER JOIN
				players p
			ON
				cp.player_id = p.id
		WHERE
			sc.name = ?
		GROUP BY p.name`, commandName).Scan(&result)

			if tx.Error != nil {
				fmt.Printf("Errro Find Player Info: %+v\n", tx.Error)
			}

			remotePage := CommandPage{
				Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
				Extension: ".html",
				Name:      commandName,
				Owners:    result,
				Cost:      sc.Cost,
				Metadata:  metadata,
			}
			buildFile := fmt.Sprintf("build/commands/%s.html", commandName)
			f, err := os.Create(buildFile)
			if err != nil {
				fmt.Printf("\nError Creating Build File: %s", err)
				return
			}
			err = command_tmpl.Execute(f, remotePage)
			if err != nil {
				fmt.Printf("err = %+v\n", err)
			}

		}
	}

}

func CommandHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	commandName := vars["command"]
	sc := stream_command.Find(db, commandName)

	var result []string
	tx := db.Raw(`
		SELECT
			p.name
		FROM
			stream_commands sc
		INNER JOIN
			commands_players cp
				ON
			sc.ID = cp.stream_command_id
			INNER JOIN
				players p
			ON
				cp.player_id = p.id
		WHERE
			sc.name = ?
		GROUP BY p.name`, commandName).Scan(&result)

	if tx.Error != nil {
		fmt.Printf("Errro Find Player Info: %+v\n", tx.Error)
	}

	remotePage := CommandPage{
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
		Name:      commandName,
		Owners:    result,
		Cost:      sc.Cost,
		Metadata:  metadata,
	}
	buildFile := fmt.Sprintf("build/commands/%s.html", commandName)
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = command_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := CommandPage{
		Domain:   "http://localhost:1992",
		Name:     commandName,
		Owners:   result,
		Cost:     sc.Cost,
		Metadata: metadata,
	}
	command_tmpl.Execute(w, page)
}
