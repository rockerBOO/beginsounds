package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

// This makes me uncomfortable
// Declaring outside of any scope
// then assigning within a scope
var db *gorm.DB
var index_tmpl *template.Template
var theme_tmpl *template.Template
var users_tmpl *template.Template
var user_tmpl *template.Template
var command_tmpl *template.Template
var commands_tmpl *template.Template
var jester_tmpl *template.Template
var audio_requests_tmpl *template.Template
var user_requests_tmpl *template.Template
var colors_tmpl *template.Template

var metadata Metadata

type HomePage struct {
	TotalCount    int64
	Domain        string
	Extension     string
	ThemesByCount []colorscheme.ThemeCount
	Metadata
	Commands []stream_command.StreamCommand
	Players  []player.Player
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	commands := stream_command.All(db)
	w.WriteHeader(http.StatusOK)
	var result []player.Player
	db.Table("players").Order("cool_points desc").Scan(&result)

	remotePage := HomePage{
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
		Commands:  commands,
		Players:   result,
		Metadata:  metadata,
	}
	buildFile := fmt.Sprintf("build/index.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = index_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := HomePage{
		Domain:   "http://localhost:1992",
		Commands: commands,
		Players:  result,
		Metadata: metadata,
	}
	index_tmpl.Execute(w, page)
}

func ColorsHandler(w http.ResponseWriter, r *http.Request) {
	totalVoteCount := colorscheme.Count(db)
	themesByCount := colorscheme.ThemesByCount(db)

	w.WriteHeader(http.StatusOK)

	page := HomePage{
		TotalCount:    totalVoteCount,
		Metadata:      metadata,
		ThemesByCount: themesByCount}

	colors_tmpl.Execute(w, page)
}

// ---------------------------=====================================

type AudioRequestsPage struct {
	Metadata
	Domain            string
	Extension         string
	TotalPlayedSounds int64
	MostPopular       []audio_request.PlayCount
}

func AudioRequestHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	// With the Go, when the function doesn't
	// exist it doesn't auto-import
	var total_played int64
	var most_popular []audio_request.PlayCount
	total_played = audio_request.TotalPlayedSounds(db)
	most_popular = audio_request.MostPopularSounds(db)

	remotePage := AudioRequestsPage{
		Domain:            "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:         ".html",
		Metadata:          metadata,
		TotalPlayedSounds: total_played,
		MostPopular:       most_popular,
	}
	buildFile := fmt.Sprintf("build/audio_requests.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = audio_requests_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := AudioRequestsPage{
		Metadata:          metadata,
		TotalPlayedSounds: total_played,
		MostPopular:       most_popular,
	}
	audio_requests_tmpl.Execute(w, page)
}

// ---------------------------=====================================

type UserRequestsPage struct {
	Domain       string
	Extension    string
	UserRequests []*soundeffect_request.SoundeffectRequest
}

func UserRequestsHandler(w http.ResponseWriter, r *http.Request) {
	// We need all unapproved requests
	sfxs, _ := soundeffect_request.Unapproved(db)

	remotePage := UserRequestsPage{
		UserRequests: sfxs,
		Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:    ".html",
	}
	buildFile := fmt.Sprintf("build/user_requests.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = user_requests_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := UserRequestsPage{
		Domain:       "http://localhost:1992",
		UserRequests: sfxs,
	}
	w.WriteHeader(http.StatusOK)
	user_requests_tmpl.Execute(w, page)
}

// ---------------------------=====================================

type JesterPage struct {
	Name string
}

func JesterHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	jester := stream_jester.CurrentJester(db)
	p := player.FindByID(db, *jester.PlayerID)
	page := JesterPage{Name: p.Name}

	jester_tmpl.Execute(w, page)
}

// ---------------------------=====================================

type Metadata struct {
	Domain string
}

func init() {
	db = database.CreateDBConn("beginsounds4")

	// We could add the other file here, easily
	index_tmpl = template.Must(template.ParseFiles("templates/index.html", "templates/sub_templates.tmpl"))
	theme_tmpl = template.Must(template.ParseFiles("templates/theme.html", "templates/sub_templates.tmpl"))
	colors_tmpl = template.Must(template.ParseFiles("templates/theme.html", "templates/sub_templates.tmpl"))
	users_tmpl = template.Must(template.ParseFiles("templates/users.html", "templates/sub_templates.tmpl"))
	user_tmpl = template.Must(template.ParseFiles("templates/user.html", "templates/sub_templates.tmpl"))
	commands_tmpl = template.Must(template.ParseFiles("templates/commands.html", "templates/sub_templates.tmpl"))
	command_tmpl = template.Must(template.ParseFiles("templates/command.html", "templates/sub_templates.tmpl"))
	jester_tmpl = template.Must(template.ParseFiles("templates/jester.html", "templates/sub_templates.tmpl"))
	audio_requests_tmpl = template.Must(template.ParseFiles("templates/audio_requests.html", "templates/sub_templates.tmpl"))
	user_requests_tmpl = template.Must(template.ParseFiles("templates/user_requests.html", "templates/sub_templates.tmpl"))
}

func main() {
	metadata = Metadata{
		Domain: "http://localhost:1992",
	}

	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)
	r.HandleFunc("/commands", CommandsHandler)
	r.HandleFunc("/users", UsersHandler)

	// r.HandleFunc("/", AudioRequestHandler)
	r.HandleFunc("/audio_requests", AudioRequestHandler)
	r.HandleFunc("/colors", ColorsHandler)
	r.HandleFunc("/commands/{command}", CommandHandler)
	r.HandleFunc("/theme/{theme}", ThemeHandler)
	r.HandleFunc("/users/{user}", UserHandler)
	r.HandleFunc("/jester", JesterHandler)
	r.HandleFunc("/user_requests", UserRequestsHandler)

	http.ListenAndServe(":1992", r)

	for {
	}
}
