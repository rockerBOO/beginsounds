package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
)

type UsersPage struct {
	ChattersToday uint
	Domain        string
	Extension     string
	Players       []player.Player
	Metadata
}

func UsersHandler(w http.ResponseWriter, r *http.Request) {
	var players []player.Player
	db.Table("players").Order("cool_points desc").Scan(&players)

	remotePage := UsersPage{
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
		Players:   players,
		Metadata:  metadata,
	}
	buildFile := fmt.Sprintf("build/users.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = users_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	w.WriteHeader(http.StatusOK)
	page := UsersPage{
		Metadata: metadata,
		Domain:   "http://localhost:1992",
		Players:  players,
	}

	users_tmpl.Execute(w, page)
}

type PlayerPage struct {
	Name         string
	CoolPoints   uint
	StreetCred   uint
	Lovers       []website_generator.LoverResult
	CommandCount int64
	Commands     []website_generator.CommandResult
	Domain       string
	Extension    string
	Metadata
}

func UserHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user := vars["user"]
	p := player.Find(db, user)
	fmt.Printf("p = %+v\n", p)
	w.WriteHeader(http.StatusOK)

	lovers := website_generator.LoverNames(db, p.ID)
	fmt.Printf("\tlovers = %+v\n", lovers)
	cmds := website_generator.CommandsAndCosts(db, p.ID)
	var cmdCount int64
	res := db.Raw(`
			SELECT count(*) FROM commands_players c
			WHERE c.player_id = ?
	`, p.ID).Count(&cmdCount)
	if res.Error != nil {
		fmt.Printf("res.Error = %+v\n", res.Error)
	}

	remotePage := PlayerPage{
		Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:    ".html",
		Metadata:     metadata,
		CoolPoints:   uint(p.CoolPoints),
		StreetCred:   uint(p.StreetCred),
		Lovers:       lovers,
		CommandCount: cmdCount,
		Commands:     cmds,
	}
	buildFile := fmt.Sprintf("build/users/%s.html", user)
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = user_tmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := PlayerPage{
		Name:         p.Name,
		Domain:       "http://localhost:1992",
		Metadata:     metadata,
		CoolPoints:   uint(p.CoolPoints),
		StreetCred:   uint(p.StreetCred),
		Lovers:       lovers,
		CommandCount: cmdCount,
		Commands:     cmds,
	}

	user_tmpl.Execute(w, page)
}
