package tee

import (
	"context"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
)

func ChatFanIn(cm1 <-chan chat.ChatMessage, cm2 <-chan chat.ChatMessage) <-chan chat.ChatMessage {
	results := make(chan chat.ChatMessage)

	go func() {
		for cm := range cm1 {
			results <- cm
		}
		for cm := range cm2 {
			results <- cm
		}
	}()
	return results
}

func FanOutString(messages <-chan string) (<-chan string, <-chan string) {
	res1 := make(chan string)
	res2 := make(chan string)

	go func() {
		for msg := range messages {
			res1 <- msg
			res2 <- msg
		}

	}()

	return res1, res2
}

func FanOut(
	ctx context.Context,
	commands <-chan chat.ChatMessage) (
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage,
	<-chan chat.ChatMessage) {

	c1 := make(chan chat.ChatMessage, 1000)
	c2 := make(chan chat.ChatMessage, 1000)
	c3 := make(chan chat.ChatMessage, 1000)
	c4 := make(chan chat.ChatMessage, 1000)
	c5 := make(chan chat.ChatMessage, 1000)
	c6 := make(chan chat.ChatMessage, 1000)
	c7 := make(chan chat.ChatMessage, 1000)
	c8 := make(chan chat.ChatMessage, 1000)
	c9 := make(chan chat.ChatMessage, 1000)
	c10 := make(chan chat.ChatMessage, 1000)
	c11 := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(c1)
		defer close(c2)
		defer close(c3)
		defer close(c4)
		defer close(c5)
		defer close(c6)
		defer close(c7)
		defer close(c8)
		defer close(c9)
		defer close(c10)
		defer close(c11)

		for c := range commands {
			select {
			case <-ctx.Done():
				return
			default:
				c1 <- c
				c2 <- c
				c3 <- c
				c4 <- c
				c5 <- c
				c6 <- c
				c7 <- c
				c8 <- c
				c9 <- c
				c10 <- c
				c11 <- c
			}
		}
	}()

	return c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11
}

func DuplicateChatMessages(
	ctx context.Context,
	commands <-chan chat.ChatMessage) (<-chan chat.ChatMessage, <-chan chat.ChatMessage) {

	// These have to have buffers on them
	c1 := make(chan chat.ChatMessage, 1000)
	sfxs := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(c1)
		defer close(sfxs)

		for c := range commands {
			select {
			case <-ctx.Done():
				return
			default:
				c1 <- c
				sfxs <- c
			}
		}
	}()

	return c1, sfxs
}
