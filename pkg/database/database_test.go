package database

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/player"
)

func TestGormConnect(t *testing.T) {
	conn := CreateDBConn("test_beginsounds3")
	var p player.Player
	conn.First(&p)
	fmt.Printf("player = %+v\n", p)
}
