package test_support

import (
	"fmt"

	"gorm.io/gorm"
)

func ClearDb(db *gorm.DB) {
	tables := []string{
		"chat_messages",
		"soundeffect_requests",
		"players_lovers",
		"audio_requests",
		"commands_players",
		"chat_messages",
		"pokemon_guesses",
		"pokemon_answers",
		"stream_jesters",
		"players",
		"stream_commands"}

	for _, table := range tables {
		// We can't TRUNCATE without dropping the constraint first
		// stmt := fmt.Sprintf("TRUNCATE %s", table)
		stmt := fmt.Sprintf("DELETE FROM %s", table)
		db = db.Exec(stmt)
		if db.Error != nil {
			fmt.Printf("Error clearing DB: %+v", db.Error)
			// panic(fmt.Sprintf("Error clearing DB: %+v", db.Error))
		}
	}
}
