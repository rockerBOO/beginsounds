package soundeffect_request

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

// Test With Start Time and End Time
// Make sure the Stream Command is Saved
// Check if the Stream command Already exists
func TestProcessStreamCommandRequests(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	// db := database.CreateDBConn("beginsounds4")
	test_support.ClearDb(db)

	sq := SoundeffectRequest{
		Name:     "puppy2",
		Url:      "https://www.youtube.com/watch?v=wfzadSG4NH0",
		Approved: true,
	}

	Save(db, &sq)

	results, _ := Approved(db)
	if len(results) != 1 {
		t.Errorf("Error Fetching Approved: %v", results)
	}

	// TODO: figure a way to only run with with a test flag like -slow
	//
	// for _, sr := range results {
	// 	sr, err := sr.ProcessRequest(db)
	// 	if err != nil {
	// 		t.Errorf("Error processing SoundeffectRequest")
	// 	}
	// 	if sr.DeletedAt == nil {
	// 		t.Errorf("Error marking SoundeffectRequest as Deleted")
	// 	}
	// }
}

func TestUnapprovedRequests(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	sq := SoundeffectRequest{
		Name: "puppy2",
		Url:  "https://www.youtube.com/watch?v=wfzadSG4NH0",
	}

	Save(db, &sq)

	results, _ := Unapproved(db)
	if len(results) != 1 {
		t.Errorf("Error Fetching Approved: %v", results)
	}
}
