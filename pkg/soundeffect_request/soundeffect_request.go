package soundeffect_request

import (
	"fmt"
	"os"
	"os/exec"
	"time"

	"gorm.io/gorm"
)

// https://medium.com/@felipedutratine/how-to-organize-the-go-struct-in-order-to-save-memory-c78afcf59ec2
// fmt.Println(unsafe.Sizeof(b)) // ordered 16 bytes
// Why do we use uint versus int
type SoundeffectRequest struct {
	ID          uint
	Url         string
	Name        string
	StartTime   string
	DeletedAt   *time.Time
	EndTime     string
	Requester   string `gorm:"-"`
	RequesterID *uint
	Approved    bool   // 1 byte
	Approver    string `gorm:"-"`
	ApproverId  *uint
}

func (sr *SoundeffectRequest) String() string {
	return fmt.Sprintf("%d %s %s %s", sr.ID, sr.Name, sr.Url, sr.Requester)
}

func (sr *SoundeffectRequest) ProcessRequest(db *gorm.DB) (*SoundeffectRequest, error) {
	cmd := "/home/begin/stream/Stream/Samples/add_sound_effect"

	args := []string{sr.Url, sr.Name}

	if sr.StartTime != "" {
		args = append(args, sr.StartTime)
	}

	if sr.EndTime != "" {
		args = append(args, sr.EndTime)
	}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		return sr, err
	}

	db.Model(sr).Update("deleted_at", time.Now())
	// fmt.Println("Successfully saved Sample")
	return sr, nil
}

func ApproveByID(db *gorm.DB, soundeffectRequestID uint, approverID uint) error {
	res := db.Table("soundeffect_requests").
		Where("ID = ?", soundeffectRequestID).
		Updates(SoundeffectRequest{Approved: true, ApproverId: &approverID})
		// Update("approved", "true")

	if res.Error != nil {
		return res.Error
	}

	return nil
}

// Do we want to return what was approved
func ApproveByName(db *gorm.DB, potentialName string, approverID uint) error {
	res := db.Table("soundeffect_requests").
		Where("name = ?", potentialName).
		Updates(SoundeffectRequest{Approved: true, ApproverId: &approverID})

	if res.Error != nil {
		return res.Error
	}

	return nil
}

func UnapproveForPlayer(db *gorm.DB, id uint, approverID uint) error {
	res := db.Delete(&SoundeffectRequest{}, id)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

// Do we want to return what was approved
func ApproveForPlayer(db *gorm.DB, requesterID uint, approverID uint) error {
	res := db.Table("soundeffect_requests").
		Where("requester_id = ?", requesterID).
		Updates(SoundeffectRequest{Approved: true, ApproverId: &approverID})

	if res.Error != nil {
		return res.Error
	}

	return nil
}

func Unapproved(db *gorm.DB) ([]*SoundeffectRequest, error) {
	var unapproved []*SoundeffectRequest
	res := db.Table("soundeffect_requests").
		Where("approved = false AND deleted_at IS NULL").
		Order("created_at DESC").
		Scan(&unapproved)
		// Limit(5).

	if res.Error != nil {
		fmt.Printf("Error Fetching Unapproved: %#v\n", res.Error)
		return unapproved, res.Error
	}

	return unapproved, nil
}

func Approved(db *gorm.DB) ([]*SoundeffectRequest, error) {
	var approved []*SoundeffectRequest
	res := db.Table("soundeffect_requests").
		Where("approved = true AND deleted_at IS NULL").
		Scan(&approved)

	if res.Error != nil {
		fmt.Printf("Error Fetching Approved: %#v\n", res.Error)
		return approved, res.Error
	}

	return approved, nil
}

func Save(db *gorm.DB, r *SoundeffectRequest) error {
	// TODO: Figure out why
	// For some reason, specifying the Table is causing it to not be sound
	// tx := db.Table("soundeffect_requests").Create(r)

	// Is Create the Wrong word?
	// Should I be saving
	tx := db.Create(r)
	if tx.Error != nil {
		fmt.Printf("Error Saving SoundeffectRequest: %+v\n", tx.Error)
		return tx.Error
	}
	return nil
}
