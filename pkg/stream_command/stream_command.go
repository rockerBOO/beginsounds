package stream_command

import (
	"fmt"
	"time"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type StreamCommand struct {
	ID        uint `gorm:"primaryKey"`
	Name      string
	Filename  string
	Theme     bool
	Cost      int `gorm:"default:1" json:"cost"`
	CreatedAt *time.Time
	Users     []string `gorm:"-" json:"permitted_users"`
}

func (sr *StreamCommand) Owners(db *gorm.DB) []string {
	var results []string
	db.Table("stream_commands").Raw(`SELECT p.name FROM stream_commands sc
						INNER JOIN commands_players cp ON sc.ID = cp.stream_command_id
						INNER JOIN players p ON p.ID = cp.player_id
						WHERE sc.name = ?`, sr.Name).Scan(&results)
	return results
}

func (sr *StreamCommand) String() string {
	return fmt.Sprintf("<StreamCommand: ID: %d Name: %s Cost: %d >",
		sr.ID, sr.Name, sr.Cost)
}

func All(db *gorm.DB) []StreamCommand {
	var commands []StreamCommand
	tx := db.Model(&StreamCommand{}).Order("cost desc").Find(&commands)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	return commands
}

func (sc *StreamCommand) IsAllowedToPlay2(db *gorm.DB, username string) bool {
	var count int64

	db.Table("stream_commands").Raw(`
		SELECT
			count(*)
		FROM
			stream_commands sc
		INNER JOIN
			commands_players cp
		ON
			sc.ID = cp.stream_command_id
		INNER JOIN
			players p
		ON
			p.ID = cp.player_id
		WHERE
			p.name = ?`,
		username).Count(&count)

	return count > 0
}

func (sc *StreamCommand) IsAllowedToPlay(db *gorm.DB, username string) bool {
	var count int64

	db.Table("stream_commands").Exec(`SELECT count(*) FROM stream_commands sc
						INNER JOIN commands_players cp ON sc.ID = cp.stream_command_id
						INNER JOIN players p ON p.ID = cp.player_id
						WHERE p.name = ?`, username).Count(&count)

	return count == 1
}

func (sc *StreamCommand) Decay(db *gorm.DB) {
	sc.Cost--
	db.Model(sc).Update("cost", sc.Cost)
}

func (sc *StreamCommand) BoughtPriceIncrease(db *gorm.DB) {
	sc.Cost++
	db.Model(sc).Update("cost", sc.Cost)
}

func (sc *StreamCommand) StolenPriceIncrease(db *gorm.DB) {
	sc.Cost = sc.Cost * 2
}

// ====================================================
// Create Stream Commands

func FindNoTheme(db *gorm.DB, name string) *StreamCommand {
	var sc StreamCommand
	res := db.Table("stream_commands").Where("name = ? AND theme is false", name).First(&sc)
	if res.Error != nil {
		// fmt.Printf("Find StreamCommand: %+v\n", res.Error)
	}
	return &sc
}

func Find(db *gorm.DB, name string) *StreamCommand {
	var sc StreamCommand
	res := db.Table("stream_commands").Where("name = ?", name).First(&sc)
	if res.Error != nil {
		// fmt.Printf("Find StreamCommand: %+v\n", res.Error)
	}
	return &sc
}

func FindOrCreate(db *gorm.DB, name string) *StreamCommand {
	var sc StreamCommand
	res := db.First(&sc, "name= ?", name)
	if res.Error != nil {
		// Is it ok to deference here??
		sc = *CreateFromName(db, name)
	}
	return &sc
}

// This is now just used for tests
func CreateFromName(db *gorm.DB, name string) *StreamCommand {
	// We could try look up the soundeffect question?
	sc := StreamCommand{Name: name, Filename: fmt.Sprintf("%s.opus", name)}

	// This does not return an ID
	res := db.Create(&sc)
	if res.Error != nil {
		fmt.Printf("Error CreateFromName: %+v\n", res.Error)
	}
	return &sc
}

// is this find or create???
func UpdateCost(db *gorm.DB, name string, cost int) *StreamCommand {
	sc := Find(db, name)
	if sc.ID != 0 {
		db.Model(sc).Update("cost", cost)
	}
	return sc
}

func CreateOrUpdate(db *gorm.DB, sc *StreamCommand) {
	db.Table("stream_commands").
		Clauses(clause.OnConflict{
			Columns:   []clause.Column{{Name: "id"}},
			DoUpdates: clause.AssignmentColumns([]string{"cost"}),
		}).Create(sc)
}

func Save(db *gorm.DB, sc *StreamCommand) {
	res := db.Create(sc)
	if res.Error != nil {
		fmt.Printf("Error Saving StreamCommand %+v\n", res.Error)
	}
}

// ====================================================

// All Stream Commands

func Count(db *gorm.DB) int64 {
	var count int64
	db = db.Model(&StreamCommand{}).Count(&count)
	if db.Error != nil {
		fmt.Printf("Error Saving %v", db.Error)
	}
	return count
}

func AllNames(db *gorm.DB) []string {
	var results []string
	db = db.Table("stream_commands").Raw("SELECT name FROM stream_commands").Scan(&results)
	if db.Error != nil {
		fmt.Printf("Error Saving %v", db.Error)
	}
	return results
}

func AllFiles(db *gorm.DB) []string {
	var results []string
	db = db.Table("stream_commands").Raw("SELECT filename FROM stream_commands").Scan(&results)
	if db.Error != nil {
		fmt.Printf("Error Saving %v", db.Error)
	}
	return results
}
