package streamgod_router

import (
	"context"
	"fmt"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

// !dropeffect @young.thug
func TestDropeffectToSpecificUser(t *testing.T) {
	ctx := context.Background()
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	cmdName := "damn"
	commands := make(chan chat.ChatMessage, 1)
	sc := stream_command.CreateFromName(db, cmdName)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	db.Create(&p2)

	msg := "!dropeffect young.thug"
	cm := chat.ChatMessage{
		PlayerID:   &p1.ID,
		PlayerName: p1.Name,
		Streamgod:  true,
		Message:    msg,
	}

	commands <- cm
	results, _, _ := Route(ctx, db, commands)
	// ) (<-chan string, <-chan string, <-chan audio_request.AudioRequest) {

	select {
	case <-time.After(50 * time.Millisecond):
		t.Errorf("We did not receive a message")
	case res := <-results:
		expectedMsg := fmt.Sprintf("@young.thug got SFXs: !%s", cmdName)
		if res != expectedMsg {
			// t.Errorf("Wrong Msg: %s", res)
		}

		if !sc.IsAllowedToPlay2(db, p2.Name) {
			// t.Errorf("Expected: %s | Received: %s", expectedMsg, res)
		}
	}

}

// !dropeffect @young.thug 20
func TestDropeffectMultipleEffectsToSpecificUser(t *testing.T) {
	ctx := context.Background()
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	commands := make(chan chat.ChatMessage, 1)

	cmdName := "damn"
	sc := stream_command.CreateFromName(db, cmdName)
	cmdName2 := "hello"
	sc2 := stream_command.CreateFromName(db, cmdName2)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	db.Create(&p2)

	fmt.Printf("sc = %+v\n", sc)
	fmt.Printf("sc2 = %+v\n", sc2)

	msg := "!dropeffect @young.thug 2"
	cm := chat.ChatMessage{
		PlayerID:   &p1.ID,
		PlayerName: p1.Name,
		Streamgod:  true,
		Message:    msg,
	}

	commands <- cm
	results, _, _ := Route(ctx, db, commands)

	select {
	case <-time.After(50 * time.Millisecond):
		t.Errorf("We did not receive a message")
	case res := <-results:
		fmt.Printf("res = %+v\n", res)
		// expectedMsg := fmt.Sprintf("@young.thug now has !%s", "damn")
		// if res != expectedMsg {
		// 	t.Errorf("Error wrong return: %s", res)
		// }

		if !sc.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("@%s should be allowed to play !%s", p2.Name, sc.Name)
		}
		if !sc2.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("@%s should be allowed to play !%s", p2.Name, sc2.Name)
		}
	}
}

// !dropeffect 10
func TestDropeffectRandomEffectsToRandomUsers(t *testing.T) {
	ctx := context.Background()
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	cmdName := "damn"
	commands := make(chan chat.ChatMessage, 2)
	cmdName2 := "hello"
	sc := stream_command.CreateFromName(db, cmdName)
	sc2 := stream_command.CreateFromName(db, cmdName2)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	p3 := player.Player{Name: "miles.davis"}
	db.Create(&p2)
	db.Create(&p3)
	fmt.Printf("p2 = %+v\n", p2)
	fmt.Printf("p2 = %+v\n", p3)
	fmt.Printf("sc = %+v\n", sc)
	fmt.Printf("sc2 = %+v\n", sc2)

	msg := "!dropeffect 2 "
	cm := chat.ChatMessage{
		PlayerID:   &p1.ID,
		PlayerName: p1.Name,
		Streamgod:  true,
		Message:    msg,
	}

	commands <- cm
	results, _, _ := Route(ctx, db, commands)
	fmt.Printf("results = %+v\n", results)

	// select {
	// case <-time.After(time.Millisecond * 50):
	// 	t.Error("Did not receive a message")
	// case res := <-results:
	// 	expectedMsg := fmt.Sprintf("@young.thug now has !%s", cmdName)
	// 	if res != expectedMsg {
	// 		t.Errorf("Expected: %s | Received: %s", expectedMsg, res)
	// 	}

	// 	if !sc.IsAllowedToPlay2(db, p2.Name) {
	// 		t.Errorf("We expected @young.thug to be allowed to play !damn")
	// 	}
	// 	if !sc2.IsAllowedToPlay2(db, p3.Name) {
	// 		t.Errorf("We expected @miles.davis to be allowed to play !hello")
	// 	}
	// }

}

func TestDropeffectRoute(t *testing.T) {
	ctx := context.Background()
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	cmdName := "damn"
	commands := make(chan chat.ChatMessage, 1)
	sc := stream_command.CreateFromName(db, cmdName)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	db.Create(&p2)
	fmt.Printf("p2 = %+v\n", p2)
	fmt.Printf("sc = %+v\n", sc)

	msg := "!dropeffect damn young.thug"
	cm := chat.ChatMessage{
		PlayerID:   &p1.ID,
		PlayerName: p1.Name,
		Streamgod:  true,
		Message:    msg,
	}

	commands <- cm
	results, _, _ := Route(ctx, db, commands)

	select {
	case <-time.After(time.Millisecond * 50):
		t.Error("Did not receive a message")
	case res := <-results:
		expectedMsg := fmt.Sprintf("@young.thug now has !%s", cmdName)
		if res != expectedMsg {
			t.Errorf("Expected: %s | Received: %s", expectedMsg, res)
		}

		if !sc.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("We expected @young.thug to be allowed to play !damn")
		}
	}

}

// !dropeffect damn 2
// Da
func TestDropeffectMultipleOfOneCommand(t *testing.T) {
	ctx := context.Background()
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	// We need to save 2 chat messages for thug and gonna
	cmdName := "damn"
	commands := make(chan chat.ChatMessage, 1)
	sc := stream_command.CreateFromName(db, cmdName)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	p3 := player.Player{Name: "gunna"}
	db.Create(&p2)
	db.Create(&p3)
	fmt.Printf("p2 = %+v\n", p2)
	fmt.Printf("p3 = %+v\n", p3)
	fmt.Printf("sc = %+v\n", sc)
	cm1 := chat.ChatMessage{PlayerID: &p2.ID}
	chat.Save(db, cm1)
	cm2 := chat.ChatMessage{PlayerID: &p3.ID}
	chat.Save(db, cm2)

	msg := "!dropeffect damn 2"
	cm := chat.ChatMessage{
		PlayerID:   &p1.ID,
		PlayerName: p1.Name,
		Streamgod:  true,
		Message:    msg,
	}

	commands <- cm
	results, _, _ := Route(ctx, db, commands)

	select {
	case <-time.After(time.Millisecond * 50):
		t.Error("Did not receive a message")
	case _ = <-results:
		// expectedMsg := "@gunna | @young.thug got !damn"
		// if res != expectedMsg {
		// 	t.Errorf("Expected: %s | Received: %s", expectedMsg, res)
		// }

		if !sc.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("We expected @young.thug to be allowed to play !damn")
		}
		if !sc.IsAllowedToPlay2(db, p3.Name) {
			t.Errorf("We expected @gunna to be allowed to play !damn")
		}
	}
}
