package website_generator

import (
	"fmt"
	"os"
	"os/exec"
)

// these sync functions can be separate
func SyncCommandSite(command string) {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		fmt.Sprintf("/home/begin/code/beginsounds/build/commands/%s.html", command),
		fmt.Sprintf("s3://beginworld/commands/%s.html", command)}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func SyncUserSite(user string) {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		fmt.Sprintf("/home/begin/code/beginsounds/build/%s.html", user),
		fmt.Sprintf("s3://beginworld/%s.html", user)}

	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func SyncSite() {
	cmd := "s3cmd"
	args := []string{
		"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"sync",
		"/home/begin/code/beginsounds/build/",
		"s3://beginworld/",
	}
	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
