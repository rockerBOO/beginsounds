package economy_router

import (
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/relationship_manager"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

func CommandInfo(db *gorm.DB, msg chat.ChatMessage, name string) string {
	c := stream_command.Find(db, name)
	owners := c.Owners(db)
	var ownerInfo string
	if len(owners) > 10 {
		ownerInfo = fmt.Sprintf("%d", len(owners))
	} else {
		ownerInfo = strings.Join(owners, ", ")
	}
	// "http://beginworld.website-us-east-1.linodeobjects.com"
	website := fmt.Sprintf("https://beginworld.website-us-east-1.linodeobjects.com/commands/%s.html", strings.ToLower(c.Name))
	// website := fmt.Sprintf("https://beginworld.us-east-1.linodeobjects.com/commands/%s.html", strings.ToLower(c.Name))
	return fmt.Sprintf("!%s Cost: %d | Owners: %v | %s", c.Name, c.Cost, ownerInfo, website)
}

func UserInfo(db *gorm.DB, playername string) string {
	cmdCount := player.FindCommandCount(db, playername)
	p := player.FindOrCreate(db, playername)
	playerLoves := relationship_manager.LoverCount(db, p.ID)
	lovesPlayer := relationship_manager.LovedCount(db, p.ID)
	website := fmt.Sprintf("https://beginworld.website-us-east-1.linodeobjects.com/users/%s.html", strings.ToLower(p.Name))
	// website := fmt.Sprintf("https://beginworld.us-east-1.linodeobjects.com/%s.html", strings.ToLower(p.Name))
	// website := fmt.Sprintf("https://mygeoangelfirespace.city/%s.html", strings.ToLower(p.Name))
	return fmt.Sprintf(
		"@%s Mana: %d | Street Cred: %d | Cool Points: %d | Command Count: %d | Love/Lovers %d/%d | %s",
		p.Name, p.Mana, p.StreetCred, p.CoolPoints, cmdCount, playerLoves, lovesPlayer, website)
}
