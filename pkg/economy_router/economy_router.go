package economy_router

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

func PermsRouter(
	ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			// We need to generate webpage on perm check
			if cmd == "!perms" || cmd == "!stats" {
				pe, parsedCmd := parser.ParseChatMessage(db, &msg)
				if pe != nil {
					results <- fmt.Sprintf("Error Fetching !perms %v", pe)
					continue MsgLoop
				}

				isTheme := parsedCmd.TargetCommand != parsedCmd.TargetUser
				if parsedCmd.TargetCommand != "" && isTheme {
					res := CommandInfo(db, msg, parsedCmd.TargetCommand)
					results <- res
					continue MsgLoop
				}

				results <- UserInfo(db, parsedCmd.TargetUser)
				continue MsgLoop
			}

		}
	}()

	return results
}

func MeRoute(ctx context.Context, db *gorm.DB, commands <-chan chat.ChatMessage) <-chan string {
	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			msgBreakdown := strings.Split(msg.Message, " ")
			cmd := strings.ToLower(strings.TrimSpace(msgBreakdown[0]))

			// We need to concat this list
			if cmd == "!jester" {
				msg := fmt.Sprintf("!pulse !spin !h !j !k !l !zoomseal !rise !pulseseal !highlight !reaction !wideputin !tallbegin !alerts !beginworld !seal")
				results <- msg
			}

			if cmd == "!me" {
				results <- UserInfo(db, msg.PlayerName)

				go func() {
					p2 := player.Find(db, msg.PlayerName)
					website_generator.CreateUserPage(db, p2)
					website_generator.SyncUserSite(p2.Name)
				}()

				continue MsgLoop
			}

		}
	}()

	return results
}
