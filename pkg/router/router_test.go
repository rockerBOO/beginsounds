package router

import (
	"context"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestRouter(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	inputChan := make(chan string, 3)

	go func() {
		inputChan <- ":tmi.twitch.tv 003 beginbotbot :This server is rather new"
		inputChan <- ":stupac62!tmi.twitch.tv PRIVMSG #beginbot :@Mccannch just a side benefit haha"
		inputChan <- ":youngthug!tmi.twitch.tv PRIVMSG #beginbot :@beginbot you're killing it"
	}()

	t.Log(inputChan)
	ctx := context.TODO()

	c := config.NewIrcConfig("beginbotbot")
	o1 := FilterChatMsgs(ctx, db, &c, inputChan)
	defer ctx.Done()

	result1 := <-o1
	if result1.PlayerName != "stupac62" {
		t.Errorf("Expected stupac62, got: %s", result1.PlayerName)
	}

	if result1.Message != "@Mccannch just a side benefit haha" {
		t.Errorf("Did Not Find PRIVMSG: %v", result1)
	}

	res2 := <-o1
	if res2.PlayerName != "youngthug" {
		t.Errorf("We expected youngthug: %s", res2.PlayerName)
	}

	select {
	case res3 := <-o1:
		t.Errorf("Got an unexpected message: %v", res3)
	default:
		t.Log("All is good, we blocking")
	}

}
