package router

import (
	"context"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

func FilterChatMsgs(
	ctx context.Context,
	db *gorm.DB,
	c *config.Config,
	messages <-chan string) <-chan chat.ChatMessage {

	UsersWeDoNotLike := []string{"nightbot"}
	// UsersWeDoNotLike := []string{"nightbot", "beginbotbot"}

	userMsgs := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(userMsgs)

		for msg := range messages {
		outside:

			select {
			case <-ctx.Done():
				return
			default:

				if parser.IsPrivmsg(msg) {
					user, m := parser.ParsePrivmsg(msg)
					p := player.FindOrCreate(db, user)
					chat_message := chat.ChatMessage{
						PlayerID:   &p.ID,
						PlayerName: user,
						Streamlord: p.Streamlord,
						Streamgod:  p.Streamgod,
						StreetCred: uint(p.StreetCred),
						CoolPoints: uint(p.CoolPoints),
						Message:    m}

					for _, u := range UsersWeDoNotLike {
						if u == user {
							break outside
						}
					}

					userMsgs <- chat_message

				} else if parser.IsPing(msg) {
					irc.Pong(c)
				}

			}
		}
	}()

	return userMsgs
}
