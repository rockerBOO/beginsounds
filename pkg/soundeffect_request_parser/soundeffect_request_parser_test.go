package soundeffect_request_parser

import (
	"fmt"
	"net/url"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
)

func TestParsingSimpleRequest(t *testing.T) {
	rawUrl := "https://www.youtube.com/watch?v=l4MNnJilyVk"
	m := fmt.Sprintf("!soundeffect %s", rawUrl)
	msg := chat.ChatMessage{PlayerName: "beginbot", Message: m}

	_, request := Parse(msg)
	u, _ := url.ParseRequestURI(rawUrl)

	if request.Url != u.String() {
		t.Errorf("WE aren't parsing Urls Properly! %s", request.Url)
	}

	if request.Requester != "beginbot" {
		t.Errorf("WE aren't parsing Requesters properly! %s", request.Requester)
	}

	if request.Name != "beginbot" {
		t.Errorf("WE aren't parsing Command Names Properly! %s", request.Name)
	}
}

func TestParsingLessSimpleRequest(t *testing.T) {
	rawUrl := "https://www.youtube.com/watch?v=l4MNnJilyVk"
	m := fmt.Sprintf("!soundeffect %s cool 00:01 00:06", rawUrl)
	msg := chat.ChatMessage{PlayerName: "beginbot", Message: m}

	_, request := Parse(msg)
	u, _ := url.ParseRequestURI(rawUrl)

	if request.Url != u.String() {
		t.Errorf("WE aren't parsing Urls Properly! %s", request.Url)
	}

	if request.Requester != "beginbot" {
		t.Errorf("WE aren't parsing Requesters properly! %s", request.Requester)
	}

	if request.Name != "cool" {
		t.Errorf("WE aren't parsing Command Names Properly! %s", request.Name)
	}

	if request.StartTime != "00:01" {
		t.Errorf("WE aren't parsing Start Time properly! %s", request.StartTime)
	}

	if request.EndTime != "00:06" {
		t.Errorf("WE aren't parsing End Time properly! %s", request.EndTime)
	}
}

func TestIsTimestamp(t *testing.T) {
	ts := "00:01"
	nts := "dskfjghsdk"
	if !isTimeStamp(ts) {
		t.Error("Not finding valid timestamps")
	}
	if isTimeStamp(nts) {
		t.Error("Marking invalid timestamps as valid")
	}
}
