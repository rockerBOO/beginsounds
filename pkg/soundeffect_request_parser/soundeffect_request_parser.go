package soundeffect_request_parser

import (
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
)

func Parse(msg chat.ChatMessage) (error, *soundeffect_request.SoundeffectRequest) {

	// This is segfaulting
	request := soundeffect_request.SoundeffectRequest{
		Requester:   msg.PlayerName,
		RequesterID: msg.PlayerID,
	}
	parts := strings.Split(msg.Message, " ")

	_, err := url.ParseRequestURI(parts[1])
	if err != nil {
		em := fmt.Sprintf("@%s passed invalid URL %+v\n", msg.PlayerName, err)
		return errors.New(em), &soundeffect_request.SoundeffectRequest{}
	}
	request.Url = parts[1]

	// If we don't include a command name
	// We assume its the Theme Song
	if len(parts) > 2 {
		for _, part := range parts[2:] {
			if isTimeStamp(part) && request.StartTime == "" {
				request.StartTime = part
			} else if isTimeStamp(part) {
				request.EndTime = part
			} else {
				request.Name = part
			}
		}
	}

	// If we haven't found a command name yet
	// Assume it's the Players Name
	if request.Name == "" {
		request.Name = msg.PlayerName
	}

	return nil, &request
}

func isTimeStamp(ts string) bool {
	var validTS = regexp.MustCompile(`^[0-9]+:[0-9]+$`)
	return validTS.MatchString(ts)
}
