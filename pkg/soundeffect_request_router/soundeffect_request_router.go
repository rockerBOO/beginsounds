package soundeffect_request_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_parser"
	"gorm.io/gorm"
)

func Route(ctx context.Context, db *gorm.DB, commands <-chan chat.ChatMessage) <-chan string {
	results := make(chan string)
	// results := make(chan string, 100)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range commands {
			select {
			case <-ctx.Done():
				return
			default:
				parsedCmd, parts, err := parser.ParseChatMessageNoJudgements(db, &msg)

				if err != nil {
					fmt.Printf("Error Parsing Command: %+v\n", err)
					continue MsgLoop
				}

				if parsedCmd.Name == "deny" && msg.Streamgod {
					err := soundeffect_request.UnapproveForPlayer(db, uint(parsedCmd.TargetAmount), *msg.PlayerID)

					if err != nil {
						fmt.Printf("Error Denying %d - %v", parsedCmd.TargetAmount, msg.PlayerID)
					}
					// We need to find the thing to Deny
					// Lets assume ID for Now
					continue MsgLoop
				}

				if parsedCmd.Name == "soundeffect" {
					if len(parts) < 2 {
						results <- "!soundeffect YOUTUBE_URL COMMAND_NAME 00:01 00:05 - Must be less than 5 second"
						continue MsgLoop
					}

					fmt.Printf("\tAttempting to save sound: %s", parsedCmd.Name)

					// Do we want to build another Parser, a more specific soundeffect request
					err, sfxRequest := soundeffect_request_parser.Parse(msg)
					if err != nil {
						fmt.Printf("err = %+v\n", err)
						continue MsgLoop
					}
					err = soundeffect_request.Save(db, sfxRequest)
					if err != nil {
						fmt.Printf("err = %+v\n", err)
						continue MsgLoop
					}

					results <- fmt.Sprintf("Thank you for you soundeffect request %s @%s. Streamlords will review it promptly",
						sfxRequest.Name, msg.PlayerName)

					continue MsgLoop
				}

				if parsedCmd.Name == "requests" && msg.Streamgod {
					unapproved, _ := soundeffect_request.Unapproved(db)

					if len(unapproved) == 0 {
						msg := fmt.Sprintf("Excellent Jobs Stream Lords No Soundeffect Requests!")
						fmt.Println(msg)
						results <- msg
						continue MsgLoop
					}

					for _, res := range unapproved {
						p := player.Player{ID: *res.RequesterID}
						tx := db.First(&p)
						if tx.Error == nil {
							req := fmt.Sprintf("ID: %d - !%s %s @%s", res.ID, res.Name, res.Url, p.Name)
							results <- req
						}
					}
					continue MsgLoop
				}

				if parsedCmd.Name == "approve" {
					if !msg.Streamlord {
						results <- fmt.Sprintf("Only Streamlords can approve @%s", msg.PlayerName)
						continue MsgLoop
					}

					if parsedCmd.TargetAmount != 0 {
						fmt.Println("\tTRYING TO TEST TARGET AMOUNT")
						err := soundeffect_request.ApproveByID(db, uint(parsedCmd.TargetAmount), *msg.PlayerID)
						if err != nil {
							fmt.Printf("Error Approving: %+v\n", err)
						}
						results <- fmt.Sprintf("Approving Soundeffect ID: %d", parsedCmd.TargetAmount)
						continue MsgLoop
					}

					if parsedCmd.TargetUser != "" {
						p := player.Find(db, parsedCmd.TargetUser)
						err := soundeffect_request.ApproveForPlayer(db, p.ID, *msg.PlayerID)
						if err != nil {
							results <- fmt.Sprintf("Error Approving: %+v\n", err)
							continue MsgLoop
						}
						results <- fmt.Sprintf("Approving Soundeffects for @%s", p.Name)
						continue MsgLoop
					}

					potentialSoundeffect := parts[1]
					err := soundeffect_request.ApproveByName(db, potentialSoundeffect, *msg.PlayerID)
					if err != nil {
						results <- fmt.Sprintf("Error Approving: %+v\n", err)
						continue MsgLoop
					}
					results <- fmt.Sprintf("Approving Soundeffects for @%s", potentialSoundeffect)
					continue MsgLoop
				}
			}
		}

	}()

	return results
}
