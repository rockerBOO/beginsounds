package soundboard

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

func Execute(
	ctx context.Context,
	db *gorm.DB,
	sfxs <-chan audio_request.AudioRequest,
) (<-chan string, <-chan string) {

	results := make(chan string)
	notifications := make(chan string)

	go func() {
		defer close(results)
		defer close(notifications)

	outside:
		for audioRequest := range sfxs {
			select {
			case <-ctx.Done():
				return
			default:
				// We need to check if always allowed to play
				// This means we can allow dynamic lengths of sample
				timer := player.FindTimerByID(db, audioRequest.PlayerID)

				// We din't a user a method for this??
				var p player.Player
				res := db.Table("players").Where("ID = ?", audioRequest.PlayerID).Find(&p)
				if res.Error != nil {
					res := fmt.Sprintf("Couldn't find User %v", p)
					fmt.Println(res)
					results <- res
					continue outside
				}

				// If you are out of Mana and NOT a Streamgod, we can't let you continue
				if p.Mana < 1 && !p.Streamgod {
					if p.Name != "" {
						res := fmt.Sprintf("@%s does not have enough Mana to play !%s",
							p.Name, audioRequest.Name)
						fmt.Println(res)
						results <- res
					}
					continue outside
				}

				if audioRequest.Notify {
					notifications <- fmt.Sprintf("@%s !%s", p.Name, audioRequest.Name)
				}

				success := PlayAudio(audioRequest, timer)
				if success {
					db.Model(&audioRequest).Update("played", true)

					// We don't subtract Mana From Stream Gods
					if !p.Streamgod {
						res := db.Exec("UPDATE players SET mana = mana - 1 WHERE ID = ?",
							audioRequest.PlayerID)
						if res.Error != nil {
							fmt.Printf("Error Updating Mana= %+v\n", res.Error)
						}
					}

				}
			}
		}
	}()

	return results, notifications
}
