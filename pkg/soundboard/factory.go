package soundboard

import (
	"errors"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func CreateAudioRequest(
	db *gorm.DB,
	potentialSample string,
	username string,
) (*audio_request.AudioRequest, error) {

	p := player.Find(db, username)

	msg := chat.ChatMessage{
		Message:    fmt.Sprintf("!%s", potentialSample),
		PlayerID:   &p.ID,
		PlayerName: username,
		Streamgod:  p.Streamgod,
		Streamlord: p.Streamlord,
	}
	return CreateAudioRequestFromChatMsg(db, &msg)
}

// WE should create more decontrcuted version
// We could start following the Gopher
// Allowing a User to Play
func CreateAudioRequestFromChatMsg(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (*audio_request.AudioRequest, error) {

	potentialSample := msg.Message[1:]
	command := stream_command.Find(db, potentialSample)
	if command.Name == "" {
		return &audio_request.AudioRequest{}, nil
	}

	if *msg.PlayerID == 0 {
		return &audio_request.AudioRequest{}, nil
	}

	filename, path := findSoundPathInfo(command.Name)
	if filename == "" || path == "" {
		// e := errors.New("Could not find the sample!")
		return &audio_request.AudioRequest{}, nil
	}

	var allowedToPlay bool

	sj := stream_jester.CurrentJester(db)
	isJester := sj.PlayerID == msg.PlayerID
	if msg.Streamlord || msg.Streamgod || isJester || msg.PlayerName == command.Name {
		allowedToPlay = true
	} else {
		allowedToPlay = player.AllowedToPlay(db, *msg.PlayerID, command.ID)
	}

	if !allowedToPlay {
		e := errors.New(
			fmt.Sprintf("@%s not allowed to play !%s",
				msg.PlayerName,
				command.Name,
			),
		)
		return &audio_request.AudioRequest{}, e
	}

	aq := audio_request.AudioRequest{
		PlayerID:     *msg.PlayerID,
		Name:         command.Name,
		StreamJester: isJester,
		Streamlord:   msg.Streamlord,
		Notify:       true,
		Filename:     filename,
		Path:         path}

	audio_request.Save(db, &aq)

	return &aq, nil
}
