package criminal_activities

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
	"gorm.io/gorm"
)

func setUpTheHeist(db *gorm.DB) (*player.Player, *player.Player, *stream_command.StreamCommand) {
	p1 := player.CreatePlayerFromName(db, "beginbot")
	p2 := player.CreatePlayerFromName(db, "young.thug")
	sc := stream_command.CreateFromName(db, "damn")
	player.AllowAccess(db, p1.ID, sc.ID)
	return p1, p2, sc
}

// Scenarios:
//    - Golden Path
// 		- Thief already owns the sound
//    - Victim doesn't own the sound
// 		- We need pass in a victim Id
// 		- We should check when this returns nothing
func TestAttemptToSteal(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	_, p2, sc := setUpTheHeist(db)

	msg := chat.ChatMessage{PlayerID: &p2.ID, PlayerName: p2.Name, Message: "!steal damn"}
	parsedCmd := parser.ParsedCommand{Name: "steal", TargetCommand: "damn"}

	res := AttemptToSteal(db, msg, &parsedCmd)

	if res != "@young.thug stole !damn from @beginbot" {
		t.Errorf("Incorrect Return Msg: %s", res)
	}

	allowed := player.AllowedToPlay(db, p2.ID, sc.ID)
	if !allowed {
		t.Errorf("young.thug should be allowed to play !damn")
	}
}

func TestAttemptToStealUnownedSound(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p2 := player.CreatePlayerFromName(db, "young.thug")

	msg := chat.ChatMessage{PlayerID: &p2.ID, PlayerName: p2.Name, Message: "!steal damn"}
	parsedCmd := parser.ParsedCommand{Name: "steal", TargetCommand: "damn"}

	res := AttemptToSteal(db, msg, &parsedCmd)
	if res != "@young.thug couldn't find anyone to steal !damn from" {
		t.Errorf("Incorrect Return Msg: %s", res)
	}

	sc := stream_command.Find(db, "damn")
	allowed := player.AllowedToPlay(db, p2.ID, sc.ID)
	if allowed {
		t.Errorf("young.thug should NOT be allowed to play !damn")
	}
}

func TestAttemptToStealAlreadyOwnedSound(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p1, _, sc := setUpTheHeist(db)

	msg := chat.ChatMessage{PlayerID: &p1.ID, PlayerName: p1.Name, Message: "!steal damn"}
	parsedCmd := parser.ParsedCommand{Name: "steal", TargetCommand: "damn"}

	res := AttemptToSteal(db, msg, &parsedCmd)

	if res != "@beginbot You can't steal what you already own !damn" {
		t.Errorf("Incorrect Return Msg: %s", res)
	}

	allowed := player.AllowedToPlay(db, p1.ID, sc.ID)
	if !allowed {
		t.Errorf("young.thug should be allowed to play !damn")
	}
}

func TestAttemptToStealFromUser(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p1 := player.CreatePlayerFromName(db, "beginbot")
	p2 := player.CreatePlayerFromName(db, "young.thug")
	p3 := player.CreatePlayerFromName(db, "metroboomin")
	sc := stream_command.CreateFromName(db, "damn")
	player.AllowAccess(db, p1.ID, sc.ID)
	player.AllowAccess(db, p3.ID, sc.ID)

	msg := chat.ChatMessage{PlayerID: &p2.ID, PlayerName: p2.Name, Message: "!steal damn @beginbot"}
	parsedCmd := parser.ParsedCommand{Name: "steal", TargetCommand: "damn", TargetUser: "beginbot"}

	res := AttemptToSteal(db, msg, &parsedCmd)

	if res != "@young.thug stole !damn from @beginbot" {
		t.Errorf("Incorrect Return Msg: %s", res)
	}

	allowed := player.AllowedToPlay(db, p2.ID, sc.ID)
	if !allowed {
		t.Errorf("young.thug should be allowed to play !damn")
	}
}

func TestAttemptToStealFromUserWhoDoesntOwnSound(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	_ = player.CreatePlayerFromName(db, "beginbot")
	p2 := player.CreatePlayerFromName(db, "young.thug")
	p3 := player.CreatePlayerFromName(db, "metroboomin")
	sc := stream_command.CreateFromName(db, "damn")
	player.AllowAccess(db, p3.ID, sc.ID)

	msg := chat.ChatMessage{PlayerID: &p2.ID, PlayerName: p2.Name, Message: "!steal damn @beginbot"}
	parsedCmd := parser.ParsedCommand{Name: "steal", TargetCommand: "damn", TargetUser: "beginbot"}

	res := AttemptToSteal(db, msg, &parsedCmd)

	if res != "@young.thug can't steal !damn, @beginbot doesn't own it!" {
		t.Errorf("Incorrect Return Msg: %s", res)
	}

	allowed := player.AllowedToPlay(db, p2.ID, sc.ID)
	if allowed {
		t.Errorf("young.thug should NOT be allowed to play !damn")
	}
}
