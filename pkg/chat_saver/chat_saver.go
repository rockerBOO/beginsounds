package chat_saver

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gorm.io/gorm"
)

// SaveChat doesn't imply its go func channel consuming + creating nature
func SaveChat(
	ctx context.Context,
	db *gorm.DB,
	dataStream <-chan chat.ChatMessage) <-chan audio_request.AudioRequest {

	// Why the magic limit of 1000
	theme_songs := make(chan audio_request.AudioRequest, 1000)

	go func() {
		defer close(theme_songs)

		for msg := range dataStream {
			select {

			case <-ctx.Done():
				return
			default:

				// This is checks if the user has chatted recently
				// plays their theme song if they haven't chatted recently
				chattedToday := chat.HasChattedToday(db, msg.PlayerName)
				if !chattedToday {
					// We have username twice because are playing the User's Theme Song
					audioRequest, err := soundboard.CreateAudioRequest(db, msg.PlayerName, msg.PlayerName)

					if err != nil {
						fmt.Printf("Error Creating Audio Request: %+v\n", err)
					}

					// As long as we find a valid sample
					if audioRequest.Name != "" {
						theme_songs <- *audioRequest
					}
				}

				// We save after since we do a check for
				// the first message above
				chat.Save(db, msg)
			}
		}
	}()

	return theme_songs
}
