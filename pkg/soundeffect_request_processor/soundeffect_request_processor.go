package soundeffect_request_processor

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

func Process(ctx context.Context, db *gorm.DB) (<-chan string, <-chan audio_request.AudioRequest) {
	results := make(chan string, 100)
	ticker := time.NewTicker(1 * time.Second)
	// ticker := time.NewTicker(30 * time.Second)

	done := make(chan bool, 1)
	audioRequests := make(chan audio_request.AudioRequest, 1000)

	go func() {
		defer close(results)
		defer close(audioRequests)

		for {
			select {
			case <-ctx.Done():
				return
			case <-done:
				return
			case _ = <-ticker.C:
				approved, err := soundeffect_request.Approved(db)
				if err != nil {
					fmt.Printf("Error Fetching the Approved Soundeffect Requests: %+v\n", err)
				}

				for _, sr := range approved {
					sr, err = sr.ProcessRequest(db)

					if err != nil {
						msg := fmt.Sprintf(
							"Error Downloading Sound: %d - %s | %+v\n",
							sr.ID,
							sr.Name,
							err)
						fmt.Printf(msg)
						// results <- msg
						continue
					}

					msg := fmt.Sprintf("New Sound Available: !%s", sr.Name)
					results <- msg

					// beginbotbot has infinite mana
					audioRequest, err := soundboard.CreateAudioRequest(db, sr.Name, "beginbotbot")
					if err != nil {
						fmt.Printf("Error Create Audio Request:	%+v\n", err)
						continue
					}

					audioRequests <- *audioRequest

					// This sometimes will fail, if the sound existed before
					// however the actual sound file was removed
					// maybe because it was annoying or offensive
					// audioRequest.Filename
					// c := stream_command.CreateFromName(db, sr.Name)
					c := stream_command.StreamCommand{Name: sr.Name,
						Filename: audioRequest.Filename}

					// We need to give user access to the sound

					// This does not return an ID
					res := db.Create(&c)
					if res.Error != nil {
						fmt.Printf("Error CreateFromName: %+v\n", res.Error)
					}

					err = player.AllowAccess(db, *sr.RequesterID, c.ID)
					if err != nil {
						fmt.Printf("Error allow User Access = %+v\n", err)
						continue
					}
					// fmt.Printf("c = %+v\n", c)
				}

			}
		}
	}()

	// ticker.Stop()
	return results, audioRequests

}
