package stream_jester

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/google/uuid"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/the_mole"
	"gorm.io/gorm"
)

// This should also leak the keys maybe?
// or Create Keys under a different name
//
// How do we make a secret?
// How do we track a winner?
// How do we give only them power for effects?
//
// Stream Jester
//   - player id
//   - secret
//   - created_at
//   - deleted_at
//
// Look up current stream Jester or Stream Gods
// or allow it for a certain amount of Mana

type StreamJester struct {
	ID        uint
	PlayerID  *uint
	Secret    string
	ChaosMode bool
	Filename  string
}

func CurrentJester(db *gorm.DB) *StreamJester {
	var sj StreamJester

	tx := db.Table("stream_jesters").Where(`
		deleted_at IS NULL
	`).Order("created_at desc").First(&sj)

	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}

	return &sj
}

func NewSecret(db *gorm.DB) *StreamJester {
	commands := stream_command.AllNames(db)

	rand.Seed(time.Now().UnixNano())
	randomIndex := rand.Intn(len(commands))
	secret := commands[randomIndex]

	// WE should save a secret
	// upload it in a file to linode
	// write code for the jester to win
	// write the code to only allow the jester to play sounds
	uuidWithHyphen := uuid.New()

	// f, err := os.Create(fmt.Sprintf("../../tmp/%s", uuidWithHyphen.String()))
	f, err := os.Create(fmt.Sprintf("tmp/%s", uuidWithHyphen.String()))
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	f.WriteString(secret)

	oldJester := CurrentJester(db)
	db.Model(&oldJester).Update("deleted_at", time.Now())

	sj := StreamJester{
		Secret:   secret,
		Filename: uuidWithHyphen.String(),
	}
	tx := db.Create(&sj)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}

	// create a new file in the tmp folder
	// filename := fmt.Sprintf("se")
	the_mole.LeakSecrets(db, uuidWithHyphen.String())

	return &sj
}
