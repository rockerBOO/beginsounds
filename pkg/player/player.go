package player

import (
	"fmt"
	"time"

	// "gitlab.com/beginbot/beginsounds/pkg/chat"

	"gorm.io/gorm"
)

type Player struct {
	ID         uint `gorm:"primaryKey"`
	CreatedAt  time.Time
	Name       string
	Streamlord bool
	Streamgod  bool
	StreetCred int `json:"street_cred"`
	CoolPoints int `json:"cool_points"`
	Mana       int
}

func FindTimerByID(db *gorm.DB, ID uint) *time.Timer {
	var p Player
	db.Table("players").Where("ID = ?", ID).First(&p)
	if p.Streamlord {
		return time.NewTimer(1 * time.Hour)
	}
	return time.NewTimer(5 * time.Second)
}

func (u *Player) String() string {
	return fmt.Sprintf("<Player: ID: %d Name: %s StreedCred: %d CoolPoints: %d Mana: %d>", u.ID,
		u.Name, u.StreetCred, u.CoolPoints, u.Mana)
}

type CommandResult struct {
	Name string
}

type CommandPlayer struct {
	ID              int
	PlayerID        int
	StreamCommandID int
}

func UpdateMana(db *gorm.DB, playerID uint, mana uint) {
	res := db.Exec("UPDATE players SET mana = mana - 1 WHERE ID = ?",
		playerID)
	if res.Error != nil {
		fmt.Printf("Error Updating Mana= %+v\n", res.Error)
	}
}

func FindCommandCount(db *gorm.DB, playerName string) int64 {
	var count int64
	res := db.Raw(`
			SELECT count(*) FROM commands_players c
			INNER JOIN players p ON p.id = c.player_id
			WHERE p.name = ?
	`, playerName).Count(&count)

	if res.Error != nil {
		fmt.Printf("res.Error = %+v\n", res.Error)
	}
	return count
}

func FindCommandsForUser(db *gorm.DB, playerName string) []CommandResult {
	var commands []CommandResult

	db.Table("stream_commands").Exec(`SELECT sc.name FROM stream_commands sc
		  INNER JOIN commands_players c ON sc.ID = c.stream_command_id
		  INNER JOIN players p ON p.ID = c.player_id
		  WHERE p.name = 'beginbot'`).Scan(&commands)

	return commands
}

// ========= //
// Find User //
// ========= //

func FindByID(db *gorm.DB, ID uint) *Player {
	var p Player
	_ = db.Table("players").Where("id = ?", ID).First(&p)
	// if res.Error != nil {
	// 	fmt.Printf("Error Player#Find: %+v\n", res.Error)
	// }
	return &p
}

func Find(db *gorm.DB, name string) *Player {
	var p Player
	res := db.Table("players").Where("name = ?", name).First(&p)
	if res.Error != nil {
		fmt.Printf("Error Player#Find: %+v\n", res.Error)
	}
	return &p
}

// ============ //
// Create Users //
// ============ //

func CreatePlayerFromName(db *gorm.DB, name string) *Player {
	p := Player{Name: name}
	db.Create(&p)
	return &p
}

func FindOrCreate(db *gorm.DB, name string) *Player {
	var p Player
	res := db.First(&p, "name= ?", name)
	if res.Error != nil {
		// Is it ok to deference here??
		p = *CreatePlayerFromName(db, name)
	}
	return &p
}

// =========== //
// All Players //
// =========== //

func Count(db *gorm.DB) int64 {
	var count int64
	db = db.Model(&Player{}).Count(&count)
	if db.Error != nil {
		fmt.Printf("Error Checking Method %v", db.Error)
	}
	return count
}

func AllNames(db *gorm.DB) []string {
	var result []string
	db = db.Model(&Player{}).Raw("SELECT name FROM players").Scan(&result)
	if db.Error != nil {
		fmt.Printf("Error Checking Method %v", db.Error)
	}
	return result
}

// ============================== //
// Lower Level Methods taking IDs //
// ============================== //

func AllowedToPlay(db *gorm.DB, pid uint, cid uint) bool {
	var count int64
	db.Table("commands_players").
		Where("player_id = ? AND stream_command_id = ?", pid, cid).
		Count(&count)
	return count == 1
}

func AllowAccess(db *gorm.DB, pid uint, cid uint) error {
	res := db.Exec("INSERT INTO commands_players (player_id,stream_command_id) VALUES (?, ?)", pid, cid)
	if res.Error != nil {
		fmt.Printf("AllowAccess Error: %+v\n", res.Error)
	}
	return res.Error
}

func RemoveAccess(db *gorm.DB, pid uint, cid uint) {
	res := db.Exec("DELETE FROM commands_players WHERE player_id = ? AND stream_command_id = ?", pid, cid)
	if res.Error != nil {
		fmt.Printf("RemoveAccess Error: %+v\n", res.Error)
	}
}

// ======================== //
// This doesn't belong here //
// ======================== //

func TotalCommandsPlayers(db *gorm.DB) int64 {
	var count int64
	res := db.Table("commands_players").Exec("SELECT count(*)").Count(&count)
	if res.Error != nil {
		panic("Can't Find commands_players")
	}
	return count
}
