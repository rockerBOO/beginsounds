package utils

import (
	"context"
	"sync"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
)

func ChatFanIn(ctx context.Context, channels ...<-chan chat.ChatMessage) <-chan chat.ChatMessage {
	multiplexedStream := make(chan chat.ChatMessage)
	var wg sync.WaitGroup

	// I am not returning of using this function
	multiplex := func(c <-chan chat.ChatMessage) {
		defer wg.Done()

		for i := range c {
			select {
			case <-ctx.Done():
				return
			case multiplexedStream <- i:
			}
		}
	}

	// Select from all the channels
	wg.Add(len(channels))

	for _, c := range channels {
		go multiplex(c)
	}

	// Wait for all the reads to complete
	go func() {
		wg.Wait()
		defer close(multiplexedStream)
	}()

	return multiplexedStream
}

func StringFanIn(ctx context.Context, channels ...<-chan string) <-chan string {
	multiplexedStream := make(chan string)
	var wg sync.WaitGroup

	// I am not returning of using this function
	multiplex := func(c <-chan string) {
		defer wg.Done()

		for i := range c {
			select {
			case <-ctx.Done():
				return
			case multiplexedStream <- i:
			}
		}
	}

	// Select from all the channels
	wg.Add(len(channels))

	for _, c := range channels {
		go multiplex(c)
	}

	// Wait for all the reads to complete
	go func() {
		wg.Wait()
		defer close(multiplexedStream)
	}()

	return multiplexedStream
}

// Fanning in is generic
// But this takes a specific type
// audio_request.AudioRequest
func AudioRequestFanIn(ctx context.Context, channels ...<-chan audio_request.AudioRequest) <-chan audio_request.AudioRequest {
	multiplexedStream := make(chan audio_request.AudioRequest)
	var wg sync.WaitGroup

	// I am not returning of using this function
	multiplex := func(c <-chan audio_request.AudioRequest) {
		defer wg.Done()

		for i := range c {
			select {
			case <-ctx.Done():
				return
			case multiplexedStream <- i:
			}
		}
	}

	// Select from all the channels
	wg.Add(len(channels))

	for _, c := range channels {
		go multiplex(c)
	}

	// Wait for all the reads to complete
	go func() {
		wg.Wait()
		defer close(multiplexedStream)
	}()

	return multiplexedStream
}
