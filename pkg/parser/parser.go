package parser

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

// This could take on more values from the chat.ChatMessage for ease of use
type ParsedCommand struct {
	Name          string
	Username      string
	StreetCred    int
	TargetUser    string
	TargetCommand string
	TargetAmount  int
	Streamlord    bool
	Streamgod     bool
}

func (p *ParsedCommand) String() string {
	return fmt.Sprintf("Name %s Username %s StreetCred %d TargetUser %s TargetCommand %s TargetAmount %d Streamgod %t Streamlord %t",
		p.Name, p.Username, p.StreetCred, p.TargetUser, p.TargetCommand, p.TargetAmount, p.Streamgod, p.Streamlord)
}

func ParseChatMessageNoJudgements(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (*ParsedCommand, []string, error) {

	var parsedCmd ParsedCommand
	parsedCmd.Username = msg.PlayerName
	parsedCmd.Streamgod = msg.Streamgod
	parsedCmd.Streamlord = msg.Streamlord

	propsGiver := player.Find(db, msg.PlayerName)

	parts := strings.Split(msg.Message, " ")

	name := string(parts[0])
	fc := string(name[0])
	if fc == "@" || fc == "!" {
		name = name[1:]
	}
	parsedCmd.Name = strings.ToLower(name)

	for _, part := range parts[1:] {

		if len(part) == 0 {
			return &parsedCmd, []string{}, errors.New("Not parts after stripping message")
		}

		fc := string(part[0])
		if fc == "@" || fc == "!" {
			part = part[1:]
		}
		part = strings.ToLower(part)

		c := stream_command.FindNoTheme(db, part)
		if c.ID != 0 {
			parsedCmd.TargetCommand = c.Name
		}

		// We are checking the Street of the TargetUser
		altN := "송써니"
		if part == altN {
			part = "ssyuni"
		}
		p := player.Find(db, part)
		if p.ID != 0 {
			parsedCmd.TargetUser = p.Name
		}

		// We should make sure intV is always positive
		intV, err := strconv.Atoi(part)
		if err == nil {
			if intV < 0 {
				return &ParsedCommand{}, parts, errors.New("You can't use negative amounts")
			}
			parsedCmd.TargetAmount = intV
		}

		// We have to make sure this is a props command
		if part == "all" {
			parsedCmd.TargetAmount = propsGiver.StreetCred
		}
	}

	return &parsedCmd, parts, nil
}

func ParseChatMessage(db *gorm.DB, msg *chat.ChatMessage) (error, *ParsedCommand) {
	var parsedCmd ParsedCommand
	parsedCmd.Username = msg.PlayerName
	parsedCmd.Streamgod = msg.Streamgod
	parsedCmd.Streamlord = msg.Streamlord

	propsGiver := player.Find(db, msg.PlayerName)

	parts := strings.Split(msg.Message, " ")

	name := string(parts[0])
	fc := string(name[0])
	if fc == "@" || fc == "!" {
		name = name[1:]
	}
	parsedCmd.Name = strings.ToLower(name)

	for _, part := range parts[1:] {
		fc := string(part[0])
		if fc == "@" || fc == "!" {
			part = part[1:]
		}
		part = strings.ToLower(part)

		c := stream_command.Find(db, part)
		if c.ID != 0 {
			parsedCmd.TargetCommand = c.Name
		}

		// We are checking the Street of the TargetUser
		altN := "송써니"
		if part == altN {
			part = "ssyuni"
		}
		p := player.Find(db, part)
		if p.ID != 0 {
			parsedCmd.TargetUser = p.Name
		}

		// We should make sure intV is always positive
		intV, err := strconv.Atoi(part)
		if err == nil {
			if intV < 0 {
				return errors.New("You can't use negative amounts"), &ParsedCommand{}
			}
			parsedCmd.TargetAmount = intV
		}

		// We have to make sure this is a props command
		if part == "all" {
			parsedCmd.TargetAmount = propsGiver.StreetCred
		}
	}

	fmt.Printf("\tParsedCommand: %v\n", parsedCmd)

	if propsGiver.StreetCred < parsedCmd.TargetAmount {
		msg := fmt.Sprintf("@%s doesn't have enough street cred %d/%d",
			parsedCmd.Username, parsedCmd.TargetAmount, parsedCmd.StreetCred)
		return errors.New(msg), &ParsedCommand{}
	}

	return nil, &parsedCmd
}

func ParsePrivmsg(msg string) (string, string) {
	args := strings.Split(msg, " ")
	user := strings.Split(args[0], "!")[0][1:]
	user_msg := strings.Join(args[3:], " ")[1:]

	// We might more trimming here
	return user, strings.TrimSpace(user_msg)
}

func IsPrivmsg(msg string) bool {
	return strings.Contains(msg, "PRIVMSG")
}

func IsPing(msg string) bool {
	return strings.Contains(msg, "PING")
}

func IsCommand(msg string) bool {
	return string(msg[0]) == "!"
}
