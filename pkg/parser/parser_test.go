package parser

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

// How should this relate to the player id and stream command id
func TestIsPing(t *testing.T) {
	data := "PING :tmi.twitch.tv"
	result := IsPing(data)

	if !result {
		t.Error("We did not Find PING")
	}
}

func TestParseCommandTarget(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	cmdName := "damn"
	stream_command.CreateFromName(db, cmdName)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!buy damn"}
	fmt.Printf("c1 = %+v\n", c1)

	_, res := ParseChatMessage(db, &c1)
	if res.TargetCommand != "damn" {
		t.Errorf("Error Parsing Target Command: %s", res.TargetCommand)
	}

	c2 := chat.ChatMessage{PlayerName: username, Message: "!buy gibberish"}

	_, res = ParseChatMessage(db, &c2)
	if res.TargetCommand != "" {
		t.Errorf("Error Parsing Target Command: %s", res.TargetCommand)
	}
}

func TestParseCommandTargetUser(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "young.thug" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}

	c2 := chat.ChatMessage{PlayerName: username, Message: "!props notrealperson"}

	_, res = ParseChatMessage(db, &c2)
	if res.TargetUser != "" {
		t.Errorf("Should have found no user: %s", res.TargetCommand)
	}
}

func TestParseCommandTargetAmount(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")

	p := player.Player{Name: username, StreetCred: 20}
	db.Create(&p)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug 10"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "young.thug" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}
	if res.TargetAmount != 10 {
		t.Errorf("Should have found Target Amount: %d", res.TargetAmount)
	}
}

func TestParseCommandPropsAll(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")
	p := player.Player{Name: username, StreetCred: 20}
	db.Create(&p)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug all"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "young.thug" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}
	if res.TargetAmount != 20 {
		t.Errorf("Should have found Target Amount: %d", res.TargetAmount)
	}
}

func TestParseCommandPropsNegative(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	player.CreatePlayerFromName(db, "young.thug")
	p := player.Player{Name: username, StreetCred: 20}
	db.Create(&p)

	c1 := chat.ChatMessage{PlayerName: username, Message: "!props young.thug -10"}

	_, res := ParseChatMessage(db, &c1)
	if res.TargetUser != "" {
		t.Errorf("Should have found user young.thug: %s", res.TargetUser)
	}
	if res.TargetAmount != 0 {
		t.Errorf("Should have found Target Amount: %d", res.TargetAmount)
	}
}
