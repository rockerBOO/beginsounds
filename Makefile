default:
	go build -gcflags="-e" .

.PHONY: web
web:
	go build -gcflags="-e" web/app.go

test:
	grc gotest ./...

t:
	GOTRACEBACK=all gotest -race ./... -v

fmt:
	go fmt ./...

test_leak:
	s3cmd get --recursive                                   \
		--access_key=73MQWUEDU7RI00UVOF21 \
		--secret_key=eWYs0AD3sjVD56sDCgN2RSP91ozQftFEVCvwb6Lv \
	 	s3://scavenger-bucket/

sync:
	# s3cmd --no-mime-magic --acl-public put build/index.html s3://beginworld/index.html
	# s3cmd --no-mime-magic --acl-public put build/index.html s3://beginworld/404.html
	# s3cmd --no-mime-magic --acl-public put build/commands.html s3://beginworld/commands.html
	s3cmd --no-mime-magic --acl-public put build/users.html s3://beginworld/users.html
	# s3cmd --no-mime-magic --delete-after put build/index.html s3://beginworld/index.html

sync_all:
	rm -rf upload/*
	mv -f build/* upload
	mkdir build/users build/commands
	s3cmd --no-mime-magic --acl-public --delete-after --recursive put upload/ s3://beginworld/

commands:
	s3cmd --no-mime-magic --acl-public --delete-after --recursive put build/commands/ s3://beginworld/commands/
	# s3cmd --delete-after sync build/commands/ s3://beginworld/commands/

css:
	s3cmd--delete-after sync ../chat_thief/build/beginworld_finance/styles/ s3://beginworld/styles/
	# s3cmd --no-mime-magic --acl-public --delete-after put ../chat_thief/build/beginworld_finance/styles/beginbot.css s3://beginworld/styles/beginbot.css
	# s3cmd --no-mime-magic --acl-public --delete-after sync build/ s3://beginworld/

sounds:
	s3cmd put -r --exclude "*" \
		--no-mime-magic --acl-public \
		--include "*.mp3"       \
		--include "*.wav"       \
		--include "*.m4a"       \
		--include "*.opus"      \
		--include "*.ogg"      \
		"/home/begin/stream/Stream/Samples/" s3://beginworld/media/
	s3cmd put -r --exclude "*" \
		--no-mime-magic --acl-public \
		--include "*.mp3"       \
		--include "*.wav"       \
		--include "*.m4a"       \
		--include "*.opus"      \
		--include "*.ogg"      \
		"/home/begin/stream/Stream/Samples/theme_songs/" s3://beginworld/media/

app:
	cd web; rm web; go build .; cd ..; web/web
