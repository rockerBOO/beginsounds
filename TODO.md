# TODO

## Top TODOs

- Migration to find what sounds aren't in the linode bucket
- Migration to find soundeffects that don't have corresponding filenames
- Migratoin to Clean Up Mana and money
- Add index.html
- Auto-approving for soundeffects, for stream lords, gods

## Bugs

- Don't charge for dropeffect
- Some themes songs are being returned from !buy maybe
- Auto allow users their own theme songs / Submitted sounds
- Too many lovers, not showing
- Ssyuni html not working

## All TODOS

- Get packs working
  - Have to structure the DB as well
- Prepare Next Linode Section: Level 2
- Cubing
- Revolution/Coup
- Donate / Give / Share
- System to toggle on Mana and Keys Leaking
- New Video For Wht is happening in the video
- make a way of explicitly setting the jester in the chat for stream gods
- Migrate the bots over and have them ignored for jester stuff
  - Build bot logic into more place and link bots to their owners
- Create a Recently Added sounds section
- Build out DB table to track all interactions for auditing, debugging
- Don't subtract money for dropeffect
- Mark thigns not to remove mana
- Build an admin channel to be able to silence any sounds easily
- Make all my commands go to beginbotbot
- Add a flag for dropping creds
- Add flag for turning off hand of the market
- Add flag for routing messages to another channel
- Add flag for not being connected to OBS
- Keep track of valid guess for pokemon
  -> also tell the people if the guess was even valid
- Collapse CreateSample and CreateAudioRequest
- !buy all
- Countdown on the cat timer
- Fix Prize Dropping
- Re-add insurance
- We are not warning users who play sounds they don't own
- Add Loving / Hating Sounds
- shias are gone
- !deny working
- !help
- !commands
- Review command pages
  - get links for user page
  - get sounds sync
  - Make sure they are being uploaded often
- Better local website
  - Stats/Events/Errors
  - Chat Messages
  - New Sounds
  - Soundeffect Requests
  - See current audio requests
    - How long until the sound will be played
    - How many in line
  - Potentially add more constraints to ChatMessage, only allow NUll MESSAGES

## Feature Requests

- !yt puppy
- !yt puppy http://jdsfhlgksjdhflg
  - We have to backfill
- !lock
  - lock you some position

## Tech Debt

- Easier to know all messages beginbotbot is sending back, and whether they are considered to be successes

## Ideas

- Theme Song Guessing Game
- Investing in Sounds

### Linode Next Steps

- Instance
- Generate SSH Key
- Create Packer instance

## Commands

- !love / !hate
  - You can love +1 person per how many love you.
  - +1 for each lover for your street cred
  - default is +3
- !lovers
  - Seeing who you love, who loves you
- !buy
  - !buy
  - !buy 20  -> the max to show all the names in Twitch
  - !buy 100 -> more than 20 it shows the amount and Cool Points
- !perms
  - !perms damn
  - !perms zanuss
- !props
  - !props
  - !props 10
  - !props @zanuss
- !soundeffect
  -> should be working now, and better
- !me
  - if you want your page refreshed !me
  - if you get access denied, wait a second and refresh (!me triggers generation
    and upload of the page)
- !color
- !pokemon
- !guess
- !props
