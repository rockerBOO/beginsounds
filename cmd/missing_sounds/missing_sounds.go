package main

import (
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
)

var bots = []string{
	"aquafunkbot",
	"r4vb0t",
	"beginbotbot",
	"cykablondesbot",
	"stallmansbot",
	"disk_bot",
	"jr_bots",
	"distributedcache",
	"opsecbot",
	"zanussbot",
	"cheb0t",
	"nomoreb0t",
	"hal9thou",
	"botriptide",
	"portaalgamingfriend",
	"800807",
	"personnormalyesplus5",
	"bigedbot",
	"futubot",
	"mrsmoofybot",
	"blortbot",
	"adengamesbot",
	"erikbotdev",
	"barfolobot",
}

func main() {
	db := database.CreateDBConn("beginsounds4")
	for _, bot := range bots {
		p := player.Find(db, bot)
		db.Model(&p).Update("bot", true)
	}
}

func oldmain() {
	// db := database.CreateDBConn("beginsounds4")
	// we need to iterate through all the sounds
	// And then we need to update the stream_command

	// // err := filepath.Walk("/home/begin/stream/Stream/Samples", UpdateStreamCommand)
	// err := filepath.Walk("/home/begin/stream/Stream/Samples",
	// 	// err := filepath.Walk("/home/begin/stream/Stream/Samples/theme_songs",
	// 	func(path string, info os.FileInfo, err error) error {
	// 		// fmt.Printf("path = %+v\n", path)
	// 		fmt.Printf("Name: %s", info.Name())
	// 		filename := info.Name()
	// 		var extension = filepath.Ext(filename)
	// 		var name = filename[0 : len(filename)-len(extension)]

	// 		// This finds files we don't want
	// 		// s := stream_command.FindOrCreate(db, name)
	// 		s := stream_command.Find(db, name)
	// 		// db.Model(&s).Updates(stream_command.StreamCommand{Filename: filename, Theme: true})
	// 		db.Model(&s).Updates(stream_command.StreamCommand{Filename: filename})

	// 		// Name is what we need
	// 		return nil
	// 	})
	// fmt.Printf("err = %+v\n", err)
}
