CREATE TABLE public.access_keys  (
    id SERIAL,
    name character varying(255) NOT NULL,
    bucket_name character varying(255) NOT NULL,
    access_key character varying(255) NOT NULL,
    secret_key character varying(255) NOT NULL,
    permissions character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT access_keys_pkey PRIMARY KEY (id)
);
