CREATE TABLE public.audio_requests(
    id SERIAL,
    player_id int references players(id),
    name character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    streamlord bool DEFAULT false NOT NULL,
    stream_jester bool DEFAULT false NOT NULL,
    notify bool DEFAULT true NOT NULL,
    played bool DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT audio_requests_pkey PRIMARY KEY (id)
);
