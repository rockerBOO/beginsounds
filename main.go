package main

import (
	"context"

	"gitlab.com/beginbot/beginsounds/cmd/obs"
	"gitlab.com/beginbot/beginsounds/notifs"
	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat_saver"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme_router"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/criminal_activities"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/economy_router"
	"gitlab.com/beginbot/beginsounds/pkg/filter"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/notification_site"
	"gitlab.com/beginbot/beginsounds/pkg/relationship_manager"
	"gitlab.com/beginbot/beginsounds/pkg/reporter"
	"gitlab.com/beginbot/beginsounds/pkg/router"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_processor"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_router"
	"gitlab.com/beginbot/beginsounds/pkg/streamgod_router"
	"gitlab.com/beginbot/beginsounds/pkg/tee"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
)

func main() {
	db := database.CreateDBConn("beginsounds4")
	// c := config.NewIrcConfig("beginbot")
	botChannelConfig := config.NewIrcConfig("beginbotbot")
	ctx := context.Background()
	themes := colorscheme.GatherColors()

	// We will need to launch another Go Routine to read
	// off from beginbotbot channel
	// and only respond to messages from Streamlords and Streamgods
	// Launch a Go Routine to collect messages from IRC
	messages := irc.ReadIrc(ctx, botChannelConfig.Conn)
	// messages := irc.ReadIrc(ctx, c.Conn)

	// Filter out only the User Chat messages from IRC
	chatMsgs := router.FilterChatMsgs(ctx, db, &botChannelConfig, messages)
	// chatMsgs := router.FilterChatMsgs(ctx, db, &c, messages)

	// Duplicate the User Messages, one to save one for further processing
	chatMsgs1, chatMsgs2 := tee.DuplicateChatMessages(ctx, chatMsgs)

	var botResponses = []<-chan string{}
	var audioRequests = []<-chan audio_request.AudioRequest{}

	// Save the User Chat Messages
	// If the User hasn't chatted today, then a Theme Request
	// Will also be played
	themeRequests := chat_saver.SaveChat(ctx, db, chatMsgs1)
	audioRequests = append(audioRequests, themeRequests)

	// This filters out just attempted user commands
	userCommands, botResponses1 := filter.RouteUserCommands(ctx, db, chatMsgs2)
	botResponses = append(botResponses, botResponses1)
	// We should be able to pass in an number to return
	uc1, uc2, uc3, uc4, uc5, uc6, uc7, uc8, uc9, uc10, uc11 := tee.FanOut(ctx, userCommands)

	// !pokemon
	ars1, botResponses11 := filter.RoutePokemonCommands(ctx, db, uc7)
	audioRequests = append(audioRequests, ars1)
	botResponses = append(botResponses, botResponses11)

	// Process audio requests and saves them in the DB to Played later
	audioRequests3, botResponses10, obsRequests := filter.AudioRequests(ctx, db, uc6)
	botResponses = append(botResponses, botResponses10)
	audioRequests = append(audioRequests, audioRequests3)

	// !color
	botResponses3 := colorscheme_router.Route(ctx, db, uc1, themes)
	botResponses = append(botResponses, botResponses3)

	// !props
	botResponses4 := economy_router.PropsRouter(ctx, db, uc10)
	botResponses = append(botResponses, botResponses4)

	// !perms
	botResponses14 := economy_router.PermsRouter(ctx, db, uc9)
	botResponses = append(botResponses, botResponses14)

	// !buy
	botResponses13 := economy_router.BuyRoute(ctx, db, uc2)
	botResponses = append(botResponses, botResponses13)

	// !me
	// !jester
	botResponses9 := economy_router.MeRoute(ctx, db, uc5)
	botResponses = append(botResponses, botResponses9)

	// !dropeffect
	// !passthejester
	adminResponses, botResponses17, audioRequests5 := streamgod_router.Route(ctx, db, uc11)
	botResponses = append(botResponses, botResponses17)
	audioRequests = append(audioRequests, audioRequests5)

	// !soundeffect
	// !requests
	// !approve
	// !deny
	botResponses5 := soundeffect_request_router.Route(ctx, db, uc3)
	botResponses = append(botResponses, botResponses5)

	// !love
	// !hate
	// !props
	botResponses8 := relationship_manager.Manage(ctx, db, uc4)
	botResponses = append(botResponses, botResponses8)

	// This runs every second to process any new soundeffect requests
	botResponses6, audioRequests2 := soundeffect_request_processor.Process(ctx, db)
	botResponses = append(botResponses, botResponses6)
	audioRequests = append(audioRequests, audioRequests2)

	// We could take in flags for this
	// We need a better way of toggling this
	// Drops Man and Street Cred
	// botResponses7 := hand_of_the_market.Serve(ctx, db)
	botResponses7 := make(chan string)
	botResponses = append(botResponses, botResponses7)

	// !steal
	botResponses12 := criminal_activities.Serve(ctx, db, uc8)
	botResponses = append(botResponses, botResponses12)

	// We need to fan in here,
	// We need to requests
	// Not just uc8
	// obsRequests
	obsCommands := tee.ChatFanIn(uc8, obsRequests)
	botResponses16, audioRequests4 := obs.TrollBegin(db, obsCommands)
	botResponses = append(botResponses, botResponses16)
	audioRequests = append(audioRequests, audioRequests4)

	// Fan-In the User Soundeffect Requests and the Automatic Theme songs
	// Then pass them to the soundboard to execute them
	playSoundRequests := utils.AudioRequestFanIn(ctx, audioRequests...)

	// This is what actually plays the sounds
	botResponses2, notifications1 := soundboard.Execute(ctx, db, playSoundRequests)
	botResponses = append(botResponses, botResponses2)

	// Fan in all the Bot Responses before Reporting
	allResults := utils.StringFanIn(ctx, botResponses...)

	// r1, r2 := tee.FanOutString(allResults)
	// Ahh the other type of message ahhhhhhhhh
	go notifs.LaunchNotifications(adminResponses)

	// Pass some messages in here
	// Also send another internal channel
	// We some type of request

	// TODO: Add flag to turn this off
	// Send beginbotbot responses back to Twitch Chat
	// reporter.Report(ctx, allResults, &c)

	reporter.Report(ctx, allResults, &botChannelConfig)

	// So we should only take in notifications here
	go notification_site.Serve(ctx, notifications1)

	for {
		select {
		case <-ctx.Done():
		default:
		}
	}
}
