// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package notifs

import (
	"flag"
	"log"
	"net/http"
)

// var addr = flag.String("addr", ":8080", "http service address")

func ServeHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "notifs/home.html")
}

// This should go somewhere else
var addr = flag.String("addr", ":8080", "http service address")

func LaunchNotifications(messages <-chan string) {
	flag.Parse()
	hub := NewHub()
	go hub.Run()
	http.HandleFunc("/", ServeHome)
	// Can I just pass in a channel here and write on the channels
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		ServeWs2(hub, w, r, messages)
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

// func main() {
// 	flag.Parse()
// 	hub := NewHub()
// 	go hub.Run()
// 	http.HandleFunc("/", serveHome)
// 	// Can I just pass in a channel here and write on the channels
// 	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
// 		serveWs(hub, w, r)
// 	})
// 	err := http.ListenAndServe(*addr, nil)
// 	if err != nil {
// 		log.Fatal("ListenAndServe: ", err)
// 	}
// }
