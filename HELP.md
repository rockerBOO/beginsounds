# HELP / FAQ

## Basic Commands

!me
!props
!buy

## Stream Jester

What is the Stream Jester?

How do I come the Stream Jester?

What can I do as Jester?

## Playing Sounds

- Streamgods can play all sounds, without costing Mana, or affecting the price
  of soundeffects

- Stream Jesters can play all sounds, but it costs them 1 Mana.

- All other users have to own the sound, and they can then play the sound for 1
  Mana.
