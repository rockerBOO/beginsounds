module gitlab.com/beginbot/beginsounds

go 1.15

require (
	github.com/agnivade/levenshtein v1.1.0
	github.com/aws/aws-sdk-go v1.34.2
	github.com/christopher-dG/go-obs-websocket v0.0.0-20200720193653-c4fed10356a5
	github.com/d4l3k/go-pry v0.0.0-20181122210047-3e3af674fe57
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f
	github.com/fatih/color v1.9.0
	github.com/go-pg/migrations v6.7.3+incompatible
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-pg/pg/v10 v10.3.2
	github.com/golang/mock v1.4.4
	github.com/google/uuid v1.1.2
	github.com/gookit/color v1.3.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.9.0
	github.com/lib/pq v1.8.0
	github.com/linode/linodego v0.23.1
	github.com/linode/terraform-provider-linode v1.13.3
	github.com/lucasb-eyer/go-colorful v1.0.3 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/veandco/go-sdl2 v0.4.4
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	gopkg.in/go-playground/colors.v1 v1.2.0
	gorm.io/driver/postgres v1.0.2
	gorm.io/gorm v1.20.2
)
